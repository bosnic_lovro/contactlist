import { Component, OnInit, Input } from '@angular/core';
import { Router, Route } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss'],
})
export class DeleteModalComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private router: Router,
    private dialog: MatDialog
  ) { }

  
  @Input() contactId: number;

  deleteUrl = `http://localhost:5000/api/deleteuser/${this.contactId}`;

  ngOnInit() {
    console.log(this.contactId);
  }

  deleteTheContact() {
    this.http.delete(`http://localhost:5000/api/deleteuser/${this.contactId}`).subscribe(() => this.dialog.closeAll());
  }

  cancleModal() {
    this.dialog.closeAll();
  }

}
