import { Component, OnInit } from '@angular/core';
import { Router, Route } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html',
  styleUrls: ['./new-contact.component.scss']
})
export class NewContactComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) { }

  createContactUrl = 'http://localhost:5000/api/createcontact';
  email: string;
  number: number;
  tag: string;
  firstName: string;
  lastName: string;
  address: string;
  bookmarked: boolean;
  emails: any[] = [{contactEmail: <string> null}];
  numbers: any[] = [{contactNumber: <number> null}];
  tags: any[] = [{contactTag: <string> null}];
  contact = {
    firstName: <string> null,
    lastName: <string> null,
    address: <string> null,
    bookmarked: <boolean> false,
    emails: <any>[],
    numbers: <any>[],
    tags: <any>[]
  };
  listOfAllContacts: any[] = [];

  ngOnInit() {
  }

  addNewContactSubmit() {

    this.contact = {
      firstName: this.firstName,
      lastName: this.lastName,
      address: this.address,
      bookmarked: this.bookmarked,
      emails: this.emails && this.emails.map(e => ({contactEmail: e.contactEmail})),
      numbers: this.numbers && this.numbers.map(n => ({contactNumber: n.contactNumber})),
      tags: this.tags && this.tags.map(t => ({contactTag: t.contactTag}))
    };

    this.http.post(this.createContactUrl, this.contact).subscribe(
      res => this.router.navigateByUrl('/home')
    );
  }

  addAnotherNumber() {
    this.numbers.push({contactNumber: <number> null});
  }

  addAnotherEmail() {
    this.emails.push({contactEmail: <string> null});
  }

  addAnotherTag() {
    this.tags.push({contactTag: <string> null});
  }

  deleteEmail(i: number) {
    if (i === 0) {
      this.emails[i] = {contactEmail: <string> null};
    } else {
      this.emails.splice(i, 1);
    }
  }

  deleteNumber(i: number) {
    if (i === 0) {
      this.numbers[i] = {contactNumber: <number> null};
    } else {
      this.numbers.splice(i, 1);
    }
  }

  deleteTag(i: number) {
    if (i === 0) {
      this.tags[i] = {contactTag: <string> null};
    } else {
      this.tags.splice(i, 1);
    }
  }

  cancleAndGoBack() {
    this.router.navigateByUrl('/home');
  }

}
