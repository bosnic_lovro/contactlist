import { Component, OnInit } from '@angular/core';
import { Router, Route } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';
import { HttpClient } from '@angular/common/http';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  listOfMyFavorites: any[];
  searchForm: FormGroup;

  constructor(
    private http: HttpClient,
    private router: Router,
    private dialog: MatDialog,
    private formBuilder: FormBuilder
  ) {
    // this.searchForm = this.formBuilder.group(
    //   {
    //     searchBtn: ['', [Validators.required]],
    //     searchValue: ['', [Validators.required]]
    //   });
  }
  public searchText: string;

  searchBtn: any;
  searchValue: any;
  getAllContactsUrl = 'http://localhost:5000/api/getallcontacts';
  isFavourite = false;
  currentTab = '';
  listOfAllContacts: any;
  listOfBookmarked: any[] = [];
  listOfSearched: any[] = [];

  ngOnInit() {
    this.searchForm = this.formBuilder.group(
      {
        searchBtn: ['', Validators.required],
        searchValue: ['', Validators.required]

      });
    this.http.get(this.getAllContactsUrl).subscribe((data: any) => {
      this.listOfAllContacts = data;
      this.listOfBookmarked = data.filter(contact => contact.bookmarked);
    });
    this.updateMyFavorites();
  }

  changeTabs(tab) {
    if (this.currentTab === tab) {
      return;
    } else {
      this.currentTab = tab;
    }
  }

  // serachContacts() {
  //   console.log(this.searchForm.value.searchBtn.value.toString());
  // }

  newContact() {
    this.router.navigateByUrl('/new-contact');
  }

  updateMyFavorites() {
    this.listOfMyFavorites = [];
    if (this.listOfAllContacts != null) {
      this.listOfAllContacts.forEach(element => {
        if (element.bookmarked === true) {
          this.listOfMyFavorites.push(element);
        }
      });
    }
  }

  deleteContact(contact) {
    const modalRef = this.dialog.open(DeleteModalComponent);
    modalRef.componentInstance.contactId = contact.id;
    this.dialog.afterAllClosed.subscribe(result => {
      this.ngOnInit();
    });
  }

  previewContact(contact) {
    this.router.navigateByUrl(`/preview-contact?id=${contact.id}`);
  }

  editContact(contact) {
    console.log(contact);
    this.router.navigateByUrl(`/edit-contact?id=${contact.id}`);
  }

  // getBy() {

  //   if (this.searchBtn === 'first') {
  //     this.http.get(`http://localhost:5000/api/getcontactsbyfirst/${this.searchValue}`)
  //       .subscribe(data => {this.listOfAllContacts = data, console.log(data); });
  //   }

  //   if (this.searchBtn === 'last') {
  //     this.http.get(`http://localhost:5000/api/getcontactsbylast/${this.searchValue}`)
  //     .subscribe(data => {this.listOfAllContacts = data, console.log(data); });
  //   }

  //   if (this.searchBtn === 'tag') {
  //     this.http.get(`http://localhost:5000/api/getcontactsbytag/${this.searchValue}`)
  //     .subscribe(data => {this.listOfAllContacts = data, console.log(data); });
  //   }
  //   console.log(this.searchBtn.toString(), this.searchBtn);
  // }

  getBy() {

    if (this.searchForm.value.searchBtn === 'first') {
      this.http.get(`http://localhost:5000/api/getcontactsbyfirst/${this.searchForm.value.searchValue}`)
        .subscribe(data => { this.listOfAllContacts = data, console.log(data); });
    }

    if (this.searchForm.value.searchBtn === 'last') {
      this.http.get(`http://localhost:5000/api/getcontactsbylast/${this.searchForm.value.searchValue}`)
        .subscribe(data => { this.listOfAllContacts = data, console.log(data); });
    }

    if (this.searchForm.value.searchBtn === 'tag') {
      this.http.get(`http://localhost:5000/api/getcontactsbytag/${this.searchForm.value.searchValue}`)
        .subscribe(data => { this.listOfAllContacts = data, console.log(data); });
    }
    console.log(this.searchForm.value.searchBtn.toString(), this.searchForm.value.searchValue);
  }

  onFormSubmit(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      this.getBy();
    }
  }
}
