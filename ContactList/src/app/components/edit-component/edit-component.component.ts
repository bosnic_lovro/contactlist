import { Component, OnInit } from '@angular/core';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';
import { HttpClient } from '@angular/common/http';
import { $ } from 'protractor';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
@Component({
    selector: 'app-edit-component',
    templateUrl: './edit-component.component.html',
    styleUrls: ['./edit-component.component.scss']
})

export class EditComponentComponent implements OnInit {

    constructor(
        private route: ActivatedRoute,
        private http: HttpClient,
        private router: Router,
        private dialog: MatDialog,
    ) { }

    searchForm: FormGroup;
    contactId = this.route.snapshot.queryParams['id'];
    getContactUrl = `http://localhost:5000/api/getcontact/${this.contactId}`;
    updateContact = 'http://localhost:5000/api/updatecontact';
    email: string;
    number: number;
    tag: string;
    firstName: string;
    lastName: string;
    address: string;
    bookmarked: boolean;
    emails: any[] = [{ contactEmail: <string>null }];
    numbers: any[] = [{ contactNumber: <number>null }];
    tags: any[] = [{ contactTag: <string>null }];
    editContact = {
        firstName: <string>null,
        lastName: <string>null,
        address: <string>null,
        bookmarked: <boolean>false,
        emails: <any>[],
        numbers: <any>[],
        tags: <any>[]
    };

    ngOnInit() {
        this.http.get(this.getContactUrl).subscribe((res: any) => { this.editContact = res; console.log(this.editContact); });
        console.log(this.editContact);
    }

    editContactSubmit() {
        this.http.put(this.updateContact, this.editContact).subscribe(res => {
            console.log(res, this.editContact);
            this.router.navigateByUrl('/home');
        });
    }

    addAnotherNumber() {
        this.editContact.numbers.push({ contactNumber: <number>null });
    }

    addAnotherEmail() {
        this.editContact.emails.push({ contactEmail: <string>null });
    }

    addAnotherTag() {
        this.editContact.tags.push({ contactTag: <string>null });
    }

    deleteEmail(i: number) {
        if (i === 0) {
            this.editContact.emails[i] = { contactEmail: <string>null };
        } else {
            this.editContact.emails.splice(i, 1);
        }
    }

    deleteNumber(i: number) {
        if (i === 0) {
            this.editContact.numbers[i] = { contactNumber: <number>null };
        } else {
            this.editContact.numbers.splice(i, 1);
        }
    }

    deleteTag(i: number) {
        if (i === 0) {
            this.editContact.tags[i] = { contactTag: <string>null };
        } else {
            this.editContact.tags.splice(i, 1);
        }
    }

    cancleAndGoBack() {
        this.router.navigateByUrl('/home');
    }
}
