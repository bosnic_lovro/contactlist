import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-preview-contact',
  templateUrl: './preview-contact.component.html',
  styleUrls: ['./preview-contact.component.scss']
})
export class PreviewContactComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  contact: any;
  contactId = this.route.snapshot.queryParams['id'];

  ngOnInit() {
    this.http.get(`http://localhost:5000/api/getcontact/${this.contactId}`).subscribe(res => { this.contact = res; });
  }

  cancleAndGoBack() {
    this.router.navigateByUrl('/home');
  }
}
