import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app-material.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NewContactComponent } from './components/new-contact/new-contact.component';
import { DeleteModalComponent } from './components/delete-modal/delete-modal.component';
import { PreviewContactComponent } from './components/preview-contact/preview-contact.component';
import { EditComponentComponent } from './components/edit-component/edit-component.component';
import { FilterPipe } from './pipe-filter';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NewContactComponent,
    DeleteModalComponent,
    PreviewContactComponent,
    EditComponentComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot()
  ],
  providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
  bootstrap: [AppComponent],
  entryComponents: [DeleteModalComponent]
})
export class AppModule { }
