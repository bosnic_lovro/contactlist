import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { NewContactComponent } from './components/new-contact/new-contact.component';
import { PreviewContactComponent } from './components/preview-contact/preview-contact.component';
import { EditComponentComponent } from './components/edit-component/edit-component.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: '', redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'new-contact',
    component: NewContactComponent
  },
  {
    path: 'preview-contact',
    component: PreviewContactComponent
  },
  {
    path: 'edit-contact',
    component: EditComponentComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
